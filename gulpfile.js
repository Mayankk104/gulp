const gulp         = require('gulp')


//GULP TASK FUNCTIONS
const watch   = require('./gulp/watch')
const clean   = require('./gulp/clean')
const zip     = require('./gulp/export')
const styles  = require('./gulp/styles')
const images  = require('./gulp/images')
const scripts = require('./gulp/srcipts')


// GULP TASKS
gulp.task('zip',zip)
gulp.task('clean',clean)
gulp.task('watch',watch)
gulp.task('images',images)
gulp.task('styles',styles)
gulp.task('scripts',scripts)
gulp.task('build',gulp.series('clean','images','styles','scripts','zip'))
gulp.task('default',gulp.series('scripts','styles','watch'));