const gulp = require('gulp')
const zip  = require('gulp-zip')


module.exports = function(done) {

    gulp.src('public/**/*')
    .pipe(zip('dist.zip'))
    .pipe(gulp.dest('./'))

    done()
}