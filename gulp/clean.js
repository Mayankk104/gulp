const del  = require('del')

const DIST_PATH = require('./global-variables').DIST_PATH


module.exports = function (done) {
    del.sync([DIST_PATH])
    done()
}
