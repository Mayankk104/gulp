const gulp  = require('gulp')

const imagemin     = require('gulp-imagemin')
const imageminPNG  = require('imagemin-pngquant')
const imageminJPEG = require('imagemin-jpeg-recompress')

const IMAGE_PATH   = require('./global-variables').IMAGE_PATH
const DIST_PATH    = require('./global-variables').DIST_PATH

module.exports = function(done){
    gulp.src(IMAGE_PATH)
    .pipe(imagemin([imagemin.svgo(),imagemin.gifsicle(),imagemin.optipng(),imagemin.jpegtran(),imageminPNG(),imageminJPEG()]))
    .pipe(gulp.dest(DIST_PATH + '/images'))

    done()
}