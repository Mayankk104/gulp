const gulp         = require('gulp')
const plumber      = require('gulp-plumber')
const browserSync  = require('browser-sync')

const sass         = require('gulp-sass')
const sourceMap    = require('gulp-sourcemaps')
const autoPrefixer = require('gulp-autoprefixer')


const SCSS_PATH   = require('./global-variables').SCSS_PATH
const DIST_PATH   = require('./global-variables').DIST_PATH



module.exports = function styles(){

    return gulp.src(SCSS_PATH)
    .pipe(plumber(function(err){console.log(err); this.emit('end')}))
    .pipe(sourceMap.init())
    .pipe(sass({outputStyle:'compressed'}))
    .pipe(autoPrefixer())
    .pipe(sourceMap.write())
    .pipe(gulp.dest(DIST_PATH + '/styles'))
    .pipe(browserSync.stream())

}