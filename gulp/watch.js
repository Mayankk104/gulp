const gulp         = require('gulp')
const browserSync  = require('browser-sync')

const SRCIPT_PATH = require('./global-variables').SRCIPT_PATH
const SCSS_PATH   = require('./global-variables').SCSS_PATH

module.exports = function watch(){
    browserSync.init({ server:'./public'});
    gulp.watch(SRCIPT_PATH,gulp.series('scripts'))
    gulp.watch(SCSS_PATH  ,gulp.series('styles'))
    gulp.watch('./*.html').on('change',browserSync.reload)
}