const gulp         = require('gulp');
const plumber      = require('gulp-plumber')
const browserSync  = require('browser-sync')

const uglify       = require('gulp-uglify')
const babel        = require('gulp-babel')
const concat       = require('gulp-concat')
const sourceMap    = require('gulp-sourcemaps')


//PATHS
const SRCIPT_PATH = require('./global-variables').SRCIPT_PATH
const DIST_PATH   = require('./global-variables').DIST_PATH

module.exports = function script() {
    return gulp.src(SRCIPT_PATH)
    .pipe(plumber(function(err){console.log(err); this.emit('end')}))
    .pipe(sourceMap.init({loadMaps:true,lagreFile:true}))
    .pipe(babel({presets:['@babel/preset-env']}))
    .pipe(concat("main.js"))
    .pipe(uglify())
    .pipe(sourceMap.write())
    .pipe(gulp.dest(DIST_PATH + '/js'))
    .pipe(browserSync.stream())  
}